# ansible-role-aurora-database #

This Playbook can be used for Creating, deleting, pause and resume of aurora database

# Role variables #
```
db_operation : create/delete/pause/resume - Default is create 
infra_name : name of platform or cloud (mandatory)
```
#### Mandatory parameters when operation is create

```infra_type : type of the infra (mandatory) - acceptable values are Cloud/Platform
mysql_version : mysql version for aurora database
instance_class : instance type for aurora database
monitoring_role_arn : iam role arn for enabling enhanced monitoring
parameter_group_cluster : Aurora database cluster Parameter group for Redsky/MDM cluster
parameter_group_instance : Aurora database instance Parameter group for Redsky/MDM cluster
replica_count : Number of replicas required for DB
root_password : DB Root password
root_username : DB Root username
subnet_group : Security group of database
security_group : subnet group for database
backup_window : backup schedule window
maintenance_window : maintenance activity window
rds_performance_insights_enable : enable/disable performance insights
```
#### Add below Mandatory parameters also if you have shards 
```
instance_class_shard : instance type for shard databases
parameter_group_shard_cluster : Aurora database cluster Parameter group for Shard cluster
parameter_group_shard_instance : Aurora database instance Parameter group for Shard cluster
```

### Example Role Calls ###

#### Example role for Create operation with Shards
```
- name: Create Databases
  include_role:
    name: galaxy-ansible-role-aurora-database
  vars:
    db_operation: create
    infra_name: "{{ platform_name }}"
    infra_type: Platform
    mysql_version: "{{ database_mysql_version }}"
    instance_class: "{{ rds_redsky_instance_type }}"
    monitoring_role_arn: "{{ stack_output_prefix.DatabaseEnhancedMonitoringRole }}"
    parameter_group_cluster: "{{ stack_output_prefix.DatabaseRedSkyParameterGroupCluster }}"
    parameter_group_instance: "{{ stack_output_prefix.DatabaseRedSkyParameterGroupInstance }}"
    replica_count: "{{ rds_replica_count|int }}"
    root_password: "{{ hostvars.localhost.database_root_password }}"
    root_username: "{{ database_root_username }}"
    security_group: "{{ stack_output_prefix.DatabaseSecurityGroup }}"
    subnet_group: "{{ stack_output_prefix.DatabaseSubnetGroup }}"
    backup_window: "{{ database_backup_window }}"
    maintenance_window: "{{ database_maintenance_window }}"
    instance_class_shard: "{{ rds_shard_instance_type }}"
    parameter_group_shard_cluster: "{{ stack_output_prefix.DatabaseShardParameterGroupCluster }}"
    parameter_group_shard_instance: "{{ stack_output_prefix.DatabaseShardParameterGroupInstance }}"
```
#### Example role for Delete operation
```
- name: Delete Databases
  include_role:
    name: galaxy-ansible-role-aurora-database
  vars:
    db_operation: delete
    infra_name: "{{ cloud_name }}"
  when:
    - operation == 'delete'
```
#### Example role for Pause operation
```
- hosts: db_config_boxes
  gather_facts: no
  vars_files:
      - vars/main.yml

  roles:
    - role: galaxy-ansible-role-aurora-database
      db_operation: pause
      when: locations|length == 1
```
#### Example role for Resume operation
```
- hosts: db_config_boxes
  gather_facts: no
  vars_files:
      - vars/main.yml

  roles:
    - role: galaxy-ansible-role-aurora-database
      db_operation: resume
      when: locations|length == 1
```
