#!/usr/bin/python
from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.ec2 import get_aws_connection_info, boto3_conn, ec2_argument_spec
import time

class AuroraCluster(object):

    def __init__(self, module):
        self.cluster_id = module.params.get('cluster_id')

        region, ec2_url, aws_connect_params = get_aws_connection_info(module, boto3=True)
        self.client = boto3_conn(
            module,
            conn_type='client',
            resource='rds',
            region=region,
            **aws_connect_params
        )
        self.changed = False
        self.failed = False
        self.failed_reason = None

        self.output = {}
        self.get_cluster_details()


    def get_cluster_details(self):
        clusters = self.client.describe_db_clusters(
            DBClusterIdentifier = self.cluster_id
        )

        if not clusters['DBClusters']:
            self.failed = True
            self.failed_reason = "Cluster Id %s not found" % self.cluster_id
            return

        self.details = clusters['DBClusters'][0]
        self.output['ClusterDetails'] = self.details


    def pause(self):
        if self.details['Status'] == 'stopped':
            return

        if self.details['Status'] != 'stopping':
            response = self.client.stop_db_cluster(
                DBClusterIdentifier = self.cluster_id
            )
            self.changed = True

        while self.details['Status'] != 'stopped':
            time.sleep(5)
            self.get_cluster_details()


    def resume(self):
        if self.details['Status'] == 'available':
            return

        if self.details['Status'] != 'starting':
            response = self.client.start_db_cluster(
                DBClusterIdentifier = self.cluster_id
            )
            self.changed = True

        while self.details['Status'] != 'available':
            time.sleep(5)
            self.get_cluster_details()


#############################################################################################

if __name__ == '__main__':
    argument_spec = ec2_argument_spec()
    argument_spec.update(dict(
        cluster_id = dict(required=True),
        operation = dict(required=True, choices=['pause', 'resume']),
    ))
    module = AnsibleModule(argument_spec=argument_spec)
    operation = module.params.get('operation')

    cluster = AuroraCluster(module)
    
    if operation == 'pause':
        cluster.pause()
    elif operation == 'resume':
        cluster.resume()

    if cluster.failed:
        module.fail_json(
            msg=cluster.failed_reason,
            changed=cluster.changed,
            **cluster.output
        )
    else:
        module.exit_json(
            changed=cluster.changed,
            **cluster.output
        )

