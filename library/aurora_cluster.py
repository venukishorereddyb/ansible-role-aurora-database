#!/usr/bin/python
from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.ec2 import get_aws_connection_info, boto3_conn, ec2_argument_spec
import time

class AuroraCluster(object):

    def __init__(self, module):
        self.smallsleep = 3
        self.bigsleep = 18
        self.allow_downtime = module.params.get('allow_downtime')
        self.state = module.params.get('state')
        self.database_type = module.params.get('database_type')
        self.stack_type = module.params.get('stack_type')
        self.stack_name = module.params.get('stack_name')
        self.mysql_version = module.params.get('mysql_version')
        self.environment_tag = module.params.get('environment_tag')
        self.boomi_contact_tag = module.params.get('boomi_contact_tag')
        self.instance_class = module.params.get('instance_class')
        self.monitoring_role_arn = module.params.get('monitoring_role_arn')
        self.parameter_group_cluster = module.params.get('parameter_group_cluster')
        self.parameter_group_instance = module.params.get('parameter_group_instance')
        self.replica_count = module.params.get('replica_count')
        self.root_username = module.params.get('root_username')
        self.root_password = module.params.get('root_password')
        self.security_group = module.params.get('security_group')
        self.subnet_group = module.params.get('subnet_group')
        self.backup_window = module.params.get('backup_window')
        self.maintenance_window = module.params.get('maintenance_window')
        self.deletion_protection = module.params.get('deletion_protection')
        self.is_active_location = module.params.get('is_active_location')
        self.rds_performance_insights_enable = module.params.get('rds_performance_insights_enable')

        # # I'm leaving this breadcrumb for future work to separate writers from readers
        # self.reader_instances = []
        # self.writer_instances = []

        self.in_progress_statuses = [
            'backing-up',
            'backtracking',
            'configuring-enhanced-monitoring',
            'configuring-iam-database-auth',
            'configuring-log-exports',
            'converting-to-vpc',
            'creating',
            'deleting',
            'maintenance',
            'modifying',
            'moving-to-vpc',
            'rebooting',
            'renaming',
            'resetting-master-credentials',
            'starting',
            'stopping',
            'storage-optimization',
            'upgrading'
        ]

        # currently not used, but saving for any future implementation
        # self.error_statuses = [
        #     'failed',
        #     'inaccessible-encryption-credentials',
        #     'incompatible-network',
        #     'incompatible-option-group',
        #     'incompatible-parameters',
        #     'incompatible-restore',
        #     'restore-error',
        #     'storage-full'
        #  ]
        # self.normal_statuses = [
        #     'available',
        #     'stopped'
        #  ]

        self.backup_retention_period = 35
        self.port = 3306

        self.region, ec2_url, aws_connect_params = get_aws_connection_info(module, boto3=True)
        self.client = boto3_conn(
            module,
            conn_type='client',
            resource='rds',
            region=self.region,
            **aws_connect_params
        )
        self.failed = False
        self.failed_reason = None
        self.changed = False

        self.global_cluster_name = "%s-%s-global-cluster" % (self.stack_name.lower(), self.database_type.lower())
        self.cluster_name = "%s-%s-cluster" % (self.stack_name.lower(), self.database_type.lower())
        self.instance_prefix = "%s-%s-instance-" % (self.stack_name.lower(), self.database_type.lower())

        if self.state != 'Absent':
            self.desired_instances = [ '%s%d' % (self.instance_prefix, i) for i in xrange(1,self.replica_count+2) ]
        else:
            self.desired_instances = []

        self.output = {}
        self.check_global_cluster()
        self.get_global_cluster_details()
        self.get_cluster_details()

    @property
    def global_cluster_exists(self):
        return bool(self.global_cluster_details)

    @property
    def is_global_cluster_member(self):
        return bool([item['DBClusterArn'] for item in self.global_cluster_members if self.region in item['DBClusterArn']])

    @property
    def cluster_exists(self):
        return bool(self.details)

    @property
    def current_instance_ids(self):
        return [i['DBInstanceIdentifier'] for i in self.instances]

    @property
    def instance_ids_vs_arn(self):
        id_vs_arn = {}
        for i in self.instances:
            instance_id = i['DBInstanceIdentifier']
            instance_arn = i['DBInstanceArn']
            id_vs_arn[instance_id] = instance_arn
        return id_vs_arn

    def get_instance_by_id(self, instance_id):
        this_instance = [i for i in self.instances if i['DBInstanceIdentifier'] == instance_id]
        if not this_instance:
            return {}
        return this_instance[0]

    def check_global_cluster(self):
        """
        Check and delete Global Cluster if no members

        Describe the Global cluster details and deletes the Global cluster if no members in the global cluster
        No action if the Global cluster not found
        Throws Exception and kills the execution if any other error
        This will help us to start with fresh setup and avoids version and other details mismatch
        """
        try:
            self.global_clusters = self.client.describe_global_clusters(
                GlobalClusterIdentifier=self.global_cluster_name
            )['GlobalClusters']
            if not self.global_clusters[0]['GlobalClusterMembers']:
                self.client.delete_global_cluster(GlobalClusterIdentifier=self.global_cluster_name)
                self._wait_for_global_cluster_completion(['available', 'deleting'], None)
        except self.client.exceptions.GlobalClusterNotFoundFault:
            # Global cluster not present
            pass

    def get_global_cluster_details(self):
        """
        Gets the details of the Global cluster

        Describes the Global cluster
        If more than one Global cluster found, raises Exception and stops execution
        Stores the details of Global cluster and its members if Global cluster exists
        If Global cluster not found then define the variables as empty
        We are using these variables to identify Global cluster existence
        """
        self.global_cluster_members = []
        try:
            self.global_clusters = self.client.describe_global_clusters(
                GlobalClusterIdentifier=self.global_cluster_name
            )['GlobalClusters']
            if len(self.global_clusters) >1:
                raise Exception('Multiple matching DB global Clusters - expecting no more than one')
            else:
                self.global_cluster_details = self.global_clusters[0]
                if self.global_clusters[0]['GlobalClusterMembers']:
                    self.global_cluster_members = self.global_cluster_details['GlobalClusterMembers']
        except self.client.exceptions.GlobalClusterNotFoundFault:
            self.global_cluster_details = {}

    def get_cluster_details(self):
        """
        Gets the details of the Aurora DB cluster

        Describes the Aurora DB cluster
        If more than one Aurora DB cluster matched, raises Exception and stops execution
        Stores the details of Aurora DB cluster
        If Aurora DB cluster not found then define the variables as empty
        We are using these variables to identify Aurora DB cluster existence
        """
        clusters = self.client.describe_db_clusters(
            Filters = [
                {'Name': 'db-cluster-id', 'Values': [self.cluster_name]}
            ]
        )['DBClusters']

        if not clusters:
            self.details = {}
        elif len(clusters) > 1:
            raise Exception('Multiple matching DB Clusters - expecting no more than one')
        else:
            self.details = clusters[0]

        self.output['DatabaseType'] = self.database_type
        self.output['ClusterArn'] = self.details.get('DBClusterArn')
        self.output['ClusterIdentifier'] = self.details.get('DBClusterIdentifier')
        self.output['ClusterStatus'] = self.details.get('Status') or 'NotFound'
        self.output['ClusterDetails'] = self.details
        self.output['DBClusterMembers'] = self.details.get('DBClusterMembers')

        self.output['ClusterActions'] = self.output.get('ClusterActions') or []
        self.output['ClusterProgress'] = self.output.get('ClusterProgress') or []

        # # I'm leaving this breadcrumb for future work to separete writers from readers
        # for member in self.output['DBClusterMembers']:
        #
        #     if member['IsClusterWriter']:
        #         print("WRITER: "+ str(member['DBInstanceIdentifier']))
        #         self.writer_instances.append(member['DBInstanceIdentifier'])
        #
        #     if not member['IsClusterWriter']:
        #         print("READER: "+ str(member['DBInstanceIdentifier']))
        #         self.reader_instances.append(member['DBInstanceIdentifier'])

        self.get_instance_details()


    def get_instance_details(self):
        """
        Gets the details of the Aurora DB cluster Instances

        Describes the Aurora DB cluster Instances
        Stores the details of Aurora DB cluster Instances
        We are using these variables to identify Aurora DB cluster Instances status to create/delete/modify
        """
        instances = self.client.describe_db_instances(
            Filters = [
                { 'Name': 'db-cluster-id', 'Values': [ self.cluster_name ] },
            ]
        )
        self.instances = instances['DBInstances']
        self.output['Instances'] = self.instances
        self.output['InstancesDesired'] = self.desired_instances
        self.output['InstanceProgress'] = self.output.get('InstanceProgress') or []
        self.output['InstanceResponse'] = self.output.get('InstanceResponse') or []

    def _wait_for_global_cluster_completion(self,in_progress_statuses, finish_status ):
        """
        This wait function checks the Global Cluster state

        It accepts the Global cluster in progress status and the finish status should cluster to be
        Compares the status of Global Cluster with in progress statuses
        If Global cluster status not in in-progress statuses, then checks with the finish status
        Returns True if Global cluster status came to finish status or returns False
        """
        time.sleep(self.smallsleep)
        self.get_global_cluster_details()
        while self.global_cluster_details.get('Status') in in_progress_statuses:
            time.sleep(smallsleep)
            self.get_global_cluster_details()

        if finish_status and self.global_cluster_details['Status'] != finish_status:
            self.failed = True
            self.failed_reason = 'Global Cluster in an invalid %s state' % self.global_cluster_details['Status']
            return False
        return True

    def _wait_for_cluster_completion(self, in_progress_statuses, finish_status):
        """
        This wait function checks the Aurora DB Cluster state

        It accepts the Aurora DB cluster in progress status and the finish status should cluster to be
        Compares the status of Aurora DB Cluster with in progress statuses
        If Aurora DB cluster status not in in-progress statuses, then checks with the finish status
        Returns True if Aurora DB cluster status came to finish status or returns False
        """
        time.sleep(self.smallsleep)
        self.get_cluster_details()
        while self.details.get('Status') in in_progress_statuses:
            time.sleep(self.smallsleep)
            self.get_cluster_details()
            self.output['ClusterProgress'] += [self.details.get('Status')]

        if finish_status and self.details['Status'] != finish_status:
            self.failed = True
            self.failed_reason = 'Cluster in an invalid %s state' % self.details['Status']
            return False

        return True


    def _wait_for_instance_completion(self, instance_id, in_progress_statuses, finish_status):
        """
        This wait function checks the Aurora DB Cluster instance status

        It accepts the cluster Instances in progress status and the finish status should cluster to be
        Compares the status of Aurora DB Cluster instance with in progress statuses
        If Aurora DB cluster instance status not in in-progress statuses, then checks with the finish status
        Returns True if Aurora DB cluster instance status came to finish status or returns False
        """
        # time.sleep(self.bigsleep)
        def is_in_progress():
            self.get_instance_details()
            this_instance = self.get_instance_by_id(instance_id)
            status = this_instance.get('DBInstanceStatus')

            print("_wait_for_instance_completion: "+ str(instance_id) +" | "+ str(status))

            self.output['InstanceProgress'] += ['%s = %s' % (instance_id, status)]
            if status in in_progress_statuses:
                return True
            return False

        while is_in_progress():
            time.sleep(self.smallsleep)

        self.get_instance_details()
        current_status = self.get_instance_by_id(instance_id).get('DBInstanceStatus')
        if finish_status and current_status != finish_status:
            self.failed = True
            self.failed_reason = 'Instance %s in an invalid state %s' % (instance_id, current_status)
            return False

        return True


    def create_or_update_cluster(self):
        """
        Creates or updates the Global cluster and Aurora DB cluster part of it

        Create Global cluster if not exists
        Creates Aurora DB cluster if not exists

        Is not possible to attach existing Aurora DB cluster as secondary cluster to Global cluster
        So in case of non-active region and existing DB is not part of Global cluster then
        we have to delete the existing cluster and create new cluster as part Global cluster
        """
        if not self.global_cluster_exists:
            self._create_global_cluster()
        if not self.cluster_exists:
            success = self._create_cluster()
        elif not self.is_global_cluster_member and not self.is_active_location:
            """Is not possible to attach existing aurora Db cluster as secondary cluster to Global cluster, 
            So in case of non active region and existing DB is not part of Global cluster then
            we have to delete the replication cluster and create new cluster as part Global cluster"""
            # self.delete_cluster()
            # success = self._create_cluster()
            raise Exception('Aurora DB cluster exists in IN-ACTIVE region, delete it to create new one under Global Database')
        else:
            success = self._update_cluster()

        if success:
            self.update_instances()

    def _create_global_cluster(self):
        """
        Creates Global cluster

        Creates empty Global cluster if Aurora DB is not available
        Creates Global cluster and Associate Aurora DB cluster if already exists
        """
        if self.global_cluster_exists and self.is_global_cluster_member:
            return
        globalparams = {
            'GlobalClusterIdentifier': self.global_cluster_name,
            'DeletionProtection': self.deletion_protection
        }
        if not self.cluster_exists:
           globalparams['Engine'] = 'aurora-mysql'
           globalparams['EngineVersion'] = self.mysql_version
           globalparams['StorageEncrypted']= True
        else:
            globalparams['SourceDBClusterIdentifier'] = self.details.get('DBClusterArn')

        self.output['global_cluster_response'] = self.client.create_global_cluster( **globalparams)
        success = self._wait_for_global_cluster_completion(self.in_progress_statuses, 'available')

        if not success: return

    def _create_cluster(self):
        """
        Creates Aurora DB cluster

        Master username and password is required only for primary cluster
        It means if no members exists for the Global cluster, then Master username and password required
        """
        self.output['ClusterActions'].append('Create Cluster')
        if self.cluster_exists:
            return

        params = {
            'BackupRetentionPeriod': self.backup_retention_period,
            'DBClusterIdentifier': self.cluster_name,
            'DBClusterParameterGroupName': self.parameter_group_cluster,
            'VpcSecurityGroupIds': [self.security_group],
            'DBSubnetGroupName': self.subnet_group,
            'DeletionProtection': self.deletion_protection,
            'Engine': 'aurora-mysql',
            'EngineVersion': self.mysql_version,
            'Port': self.port,
            'PreferredBackupWindow': self.backup_window,
            'PreferredMaintenanceWindow': self.maintenance_window,
            'Tags': [
                { "Key": "BoomiContact", "Value": "%s" % self.boomi_contact_tag },
                { "Key": "Name", "Value": "%s-%s-databasecluster" %(self.stack_type, self.stack_name) },
                { "Key": "Customer", "Value": "Boomi" },
                { "Key": "Environment", "Value": self.environment_tag },
                { "Key": "StackName", "Value": self.stack_name },
                { "Key": "StackType", "Value": self.stack_type },
                { "Key": "Role", "Value": "Database" }
            ],
            'StorageEncrypted': True,
            'KmsKeyId': 'alias/aws/rds',
            'EnableCloudwatchLogsExports': ['slowquery', 'audit', 'general', 'error' ],
            'GlobalClusterIdentifier': self.global_cluster_name,
        }
        # Master username and password are required only if global cluster is empty
        if not self.global_cluster_members:
            params['MasterUsername'] = self.root_username
            params['MasterUserPassword'] = self.root_password

        self.output['cluster_response'] = self.client.create_db_cluster( **params )
        self.changed = True
        success = self._wait_for_cluster_completion(self.in_progress_statuses, 'available')
        if not success: return

        self.update_instances()


    def _update_cluster(self):
        """
        Updates Aurora DB cluster

        As of now doing updates to Aurora Db Cluster with no downtime changes
        Skipping the changes that require downtime
        """
        self.output['ClusterActions'].append('Update Cluster')
        if not self.cluster_exists:
            raise Exception('Tried to update a non-existing cluster')

        # Adding this call here in case a dbparameter group update is modifying the cluster, eg - BOOMI-39023
        self._wait_for_cluster_completion(self.in_progress_statuses, 'available')

        changes = {
            # No downtime
            'BackupRetentionPeriod': self.backup_retention_period,
            'DeletionProtection': self.deletion_protection,
            'PreferredBackupWindow': self.backup_window,
            'PreferredMaintenanceWindow': self.maintenance_window,
            'VpcSecurityGroupIds': [self.security_group],
            'CloudwatchLogsExportConfiguration': {
                'EnableLogTypes': [ 'slowquery', 'audit', 'general', 'error' ]
            },
        }

        ####################################################
        # Skipping updates due to issues or downtime
        ####################################################
        # ONLY DO IF CHANGE IS NEEDED - HOW TO KNOW CHANGE IS NEEDED?
        #'MasterUserPassword': self.root_password,

        # REBOOT NEEDED TO TAKE EFFECT
        #'DBClusterParameterGroupName': self.parameter_group_cluster,

        # DOWNTIME
        #if self.details['EngineVersion'] != self.mysql_version:
        #    changes['EngineVersion'] = self.mysql_version

        self.output['cluster_response'] = []
        self.output['cluster_response'].append(
            self.client.modify_db_cluster(
                DBClusterIdentifier = self.cluster_name,
                ApplyImmediately = True,
                **changes
            )
        )

        self.output['cluster_response'].append(
            self.client.add_tags_to_resource(
                ResourceName = self.output['ClusterArn'],
                Tags=[
                    {
                        'Key': 'BoomiContact',
                        'Value': self.boomi_contact_tag
                    },
                ]
            )
        )

        success = self._wait_for_cluster_completion(self.in_progress_statuses, 'available')
        return success

    def _delete_global_cluster(self):
        """
        Deletes Global cluster
        """
        if not self.global_cluster_exists:
            return
        self.client.delete_global_cluster(
            GlobalClusterIdentifier=self.global_cluster_name
        )
    def delete_cluster(self):
        """
        Deletes Aurora DB cluster

        Promote Aurora DB cluster to standalone DB cluster before deleting if its Replica cluster
        Remove from Global cluster before deleting if its part of Global cluster
        Delete all secondary clusters before removing primary cluster from Global Cluster
        """
        self.output['ClusterActions'].append('Delete Cluster')
        if not self.cluster_exists:
            return
        #Promote to Standalone DB if its Replica DB cluster or part of Global cluster
        if 'ReplicationSourceIdentifier' in self.details and not self.is_global_cluster_member:
            self.client.promote_read_replica_db_cluster(
                DBClusterIdentifier=self.cluster_name
            )
        if self.is_global_cluster_member:
            self.client.remove_from_global_cluster(
                GlobalClusterIdentifier = self.global_cluster_name,
                DbClusterIdentifier=self.details.get('DBClusterArn')
            )
        success = self._wait_for_cluster_completion(self.in_progress_statuses, 'available')
        if not success: return
        #End of promoting to standalone DB

        for instance_id in self.current_instance_ids:
            self.delete_instance(instance_id)

        for instance_id in self.current_instance_ids:
            self._wait_for_instance_completion(
                instance_id,
                self.in_progress_statuses,
                None
            )

        self.output['cluster_response'] = self.client.delete_db_cluster(
            DBClusterIdentifier = self.cluster_name,
            SkipFinalSnapshot = True
        )
        self.changed = True
        self._wait_for_cluster_completion(['available', 'deleting'], None)

        #Verify and Delete Global cluster if its members are empty
        self.get_global_cluster_details()
        if not self.global_cluster_members:
            self._delete_global_cluster()
            self._wait_for_global_cluster_completion(['available', 'deleting'], None)


    def update_instances(self):
        """
        Updating the number of instances per cluster and updating properties of each instances

        Updates each instance properties which are part of Aurora DB cluster
        Creates new instances if desired number of instances are not present
        Deletes existing instances if desired number of instances are less than existing instances count
        """
        actions = {}
        for instance_id in self.desired_instances:

            print("\n---------\ninstance_id\n"+ str(instance_id) +"\n---------\n")

            if instance_id not in self.current_instance_ids:
                actions[instance_id] = {
                    'action': self.create_instance,
                    'progress_statuses': self.in_progress_statuses,
                    'final_status': 'available'
                }
            else:

                # Adding this call here in case a dbparameter group update is modifying the cluster, eg - BOOMI-39023
                self._wait_for_instance_completion(
                    instance_id,
                    self.in_progress_statuses,
                    'available'
                )

                actions[instance_id] = {
                    'action': self.update_instance,
                    'progress_statuses': self.in_progress_statuses,
                    'final_status': 'available'
                }

        for instance_id in self.current_instance_ids:
            if instance_id not in self.desired_instances:
                actions[instance_id] = {
                    'action': self.delete_instance,
                    'progress_statuses': self.in_progress_statuses,
                    'final_status': None
                }

        for instance_id, values in actions.items():
            values['action'](instance_id)

        for instance_id, values in actions.items():
            self._wait_for_instance_completion(
                instance_id,
                self.in_progress_statuses,
                values['final_status']
            )


    def create_instance(self, instance_id):
        """
        Creates Aurora DB instance

        Creates new instance under Aurora DB cluster with the given name
        """
        self.output['ClusterActions'].append('Create Instance %s' % instance_id)
        self.output['InstanceResponse'].append(
            self.client.create_db_instance(
                DBClusterIdentifier = self.cluster_name,
                DBInstanceIdentifier = instance_id,
                AutoMinorVersionUpgrade = False,
                CopyTagsToSnapshot = True,
                DBInstanceClass = self.instance_class,
                DBParameterGroupName = self.parameter_group_instance,
                DBSubnetGroupName = self.subnet_group,
                Engine = 'aurora-mysql',
                EngineVersion = self.mysql_version,
                MonitoringInterval = 1,
                MonitoringRoleArn = self.monitoring_role_arn,
                PreferredMaintenanceWindow = self.maintenance_window,
                PubliclyAccessible = False,
                EnablePerformanceInsights=self.rds_performance_insights_enable,
                Tags = [
                    { "Key": "BoomiContact", "Value": "%s" % self.boomi_contact_tag },
                    { "Key": "Name", "Value": "%s-%s-database" %(self.stack_type, self.stack_name) },
                    { "Key": "Customer", "Value": "Boomi" },
                    { "Key": "Environment", "Value": self.environment_tag },
                    { "Key": "StackName", "Value": self.stack_name },
                    { "Key": "StackType", "Value": self.stack_type },
                    { "Key": "Role", "Value": "Database" }
                ],
            )
        )


    def update_instance(self, instance_id):
        """
        Updates Aurora DB cluster instance properties

        Some changes don't require downtime.
        Downtime changes are hidden behind --allow-downtime flag
        no downtime and downtime changes are applying separately to the instance
        """
        self.output['ClusterActions'].append('Update Instance %s' % instance_id)
        this_instance = self.get_instance_by_id(instance_id)

        immediate_changes = {
            # No downtime
            'MonitoringRoleArn': self.monitoring_role_arn,
            'PreferredMaintenanceWindow': self.maintenance_window,
            'EnablePerformanceInsights': self.rds_performance_insights_enable
         }

        downtime_changes = {
            # POSSIBLE DOWNTIME - hidden behind --allow-downtime
            'DBInstanceClass': self.instance_class,
        }

        # maintenance_changes = {
        #     # Applied during next maintenance window
        #     'CACertificateIdentifier': 'rds-ca-2019'
        # }

        ####################################################
        # Skipping updates due to issues or downtime
        ####################################################

        # Reboot needed
        #if len(this_instance['DBParameterGroups']) == 1 and \
        #    this_instance['DBParameterGroups'][0]['DBParameterGroupName'] != self.parameter_group_instance:
        #    changes['DBParameterGroupName'] = self.parameter_group_instance

        # Outage
        #if this_instance['EngineVersion'] != self.mysql_version:
        #    changes['EngineVersion'] = self.mysql_version

        self.output['InstanceResponse'].append(
            self.client.modify_db_instance(
                DBInstanceIdentifier = instance_id,
                ApplyImmediately = True,
                **immediate_changes
            )
        )

        # STASH THINGS BEHIND "allow_downtime" FLAG
        if self.allow_downtime:

            print("DOWNTIME: "+ str(self.allow_downtime) +" - "+ str(instance_id))

            self.output['InstanceResponse'].append(
                self.client.modify_db_instance(
                    DBInstanceIdentifier = instance_id,
                    ApplyImmediately = True,
                    **downtime_changes
                )
            )
            '''
            DB instances reboots if rds_performance_schema_enabled
            and perameter group status is 'pending-reboot' and allow_downtime is parsed
            '''
            if this_instance['DBParameterGroups'][0]['ParameterApplyStatus'] == 'pending-reboot':
                self.client.reboot_db_instance(DBInstanceIdentifier=instance_id)

        # self.output['InstanceResponse'].append(
        #     self.client.modify_db_instance(
        #         DBInstanceIdentifier = instance_id,
        #         ApplyImmediately = False,
        #         **maintenance_changes
        #         )
        #     )

        self.output['InstanceResponse'].append(
            self.client.add_tags_to_resource(
                ResourceName = self.instance_ids_vs_arn[instance_id],
                Tags=[
                    {
                        'Key': 'BoomiContact',
                        'Value': self.boomi_contact_tag
                    },
                ]
            )
        )

        time.sleep(self.bigsleep)


    def delete_instance(self, instance_id):
        """
        Deletes Aurora DB cluster instance

        Deletes the given aurora cluster instance
        """
        self.output['ClusterActions'].append('Delete Instance %s' % instance_id)
        this_instance = self.get_instance_by_id(instance_id)
        if not this_instance:
            return

        self.output['InstanceResponse'].append(
            self.client.delete_db_instance(
                DBInstanceIdentifier = instance_id,
                SkipFinalSnapshot = True
            )
        )



#############################################################################################

if __name__ == '__main__':
    argument_spec = ec2_argument_spec()
    argument_spec.update(dict(
        stack_name = dict(required=True),
        database_type = dict(required=True, choices=['RedSky', 'MDM'] + ['Shard%02d'%i for i in xrange(1,21)]),
        stack_type=dict(choices=['Platform', 'Cloud']),
        state = dict(required=True, choices=['Present', 'Absent']),

        allow_downtime = dict(type='bool', required=False),
        mysql_version = dict(),
        environment_tag = dict(),
        boomi_contact_tag = dict(),
        instance_class = dict(),
        instance_class_shard = dict(),
        monitoring_role_arn = dict(),
        parameter_group_cluster = dict(),
        parameter_group_instance = dict(),
        parameter_group_shard_cluster = dict(),
        parameter_group_shard_instance = dict(),
        replica_count = dict(type='int'),
        root_username = dict(),
        root_password = dict(no_log=True),
        security_group = dict(),
        subnet_group = dict(),
        backup_window = dict(),
        maintenance_window = dict(),
        is_active_location = dict(type='bool'),
        deletion_protection = dict(type='bool', default=False),
        rds_performance_insights_enable=dict(type='bool'),
    ))
    required_if=[
        [ "state", "Present", [
            'stack_type',
            'mysql_version',
            'environment_tag',
            'boomi_contact_tag',
            'instance_class',
            'monitoring_role_arn',
            'parameter_group_cluster',
            'parameter_group_instance',
            'replica_count',
            'root_username',
            'root_password',
            'security_group',
            'subnet_group',
            'backup_window',
            'maintenance_window',
            'rds_performance_insights_enable'
        ] ]
    ]

    module = AnsibleModule(argument_spec=argument_spec, required_if=required_if)
    state = module.params.get('state')

    cluster = AuroraCluster(module)
    if state == 'Absent':
        cluster.delete_cluster()
    else:
        cluster.create_or_update_cluster()

    if cluster.failed:
        module.fail_json(
            msg=cluster.failed_reason,
            changed=cluster.changed,
            **cluster.output
        )
    else:
        module.exit_json(
            changed=cluster.changed,
            **cluster.output
        )
